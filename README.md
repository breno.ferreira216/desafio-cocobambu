# Desafio QA

# Descrição
Realização do desafio proposto pelo cocobambu com intuito da execução de testes de caixa preta manuais e automatizados seguindo o roteiro. Nesse roteiro de execução foi pedido a realização dos testes manuais de três cenários que seriam eles 'Login', 'Cadastro' e 'Recuperação de Senha', os testes foram realizados visando vários cenários possíveis cuidando dos testes de usabilidade, caixa preta, exploratório e segurança, no total foram 21 páginas contendo os testes feitos em bdd com status de falha ou sucesso.

Na automação, foi feita com junit, cucumber como framework e selenium pra execução dos testes automatizados, no roteiro de teste foi dito pra automatizar dois cenários e os cenários que foram automatizados foram o de login e o de cadastro.
# Execução
## Testes manuais
- [ ] [Cenário de teste - Login]
- [ ] [Cenário de teste - Cadastro]
- [ ] [Cenário de teste - Resetar senha]

* Cenários de teste na pasta "casos de teste".

## Testes automatizados
- [ ] [Cenário de teste - Login]
- [ ] [Cenário de teste - Cadastro]

[Execução dos testes automatizados](https://www.loom.com/share/6633f0c29f17469188d2bdace5f8c787)


# Contato
* E-mail: breno.ferreira216@gmail.com
* Telefone: (61) 9 9220-1623 (Whatsapp/Ligação)
* LinkedIn: https://www.linkedin.com/in/brenofgoncalves/

