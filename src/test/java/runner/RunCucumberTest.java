package runner;

import config.GerenciadorJava;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;


@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        glue = "steps",
        monochrome = true
)
public class RunCucumberTest {
    @AfterClass
    public static void fecharBrowser() {
        GerenciadorJava.killDriver();
    }
}
