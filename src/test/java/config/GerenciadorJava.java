package config;

import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class GerenciadorJava {
    private static ChromeDriver driver;

    private static ChromeDriver createDriver() {
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\breno\\Documents\\ChromeDriver\\chromedriver.exe");

        ChromeOptions options = new ChromeOptions();
        options.addArguments("--remote-allow-origins=*");

         driver = new ChromeDriver(options);
         driver.manage().window().maximize();
         return driver;
    }

    public static ChromeDriver getDriver() {
        if (driver == null)
            createDriver();
        return driver;
    }

    public static void killDriver() {
        if (driver!=null) {
            driver.quit();
            driver = null;
        }
    }
}
