package steps;

import config.GerenciadorJava;
import cucumber.api.java.es.Dado;
import cucumber.api.java.it.Quando;
import cucumber.api.java.pt.Entao;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.Sleeper;
import org.openqa.selenium.support.ui.WebDriverWait;
import page.PageObject;
import page.PageTeste;

public class desafio_step {
    PageTeste elementos = new PageTeste();
    PageObject po = new PageObject();
    GerenciadorJava chromedriver = new GerenciadorJava();
    public WebDriver driver;
    public Select select;
    public Sleeper snoop;
    public WebDriverWait wait;

@Dado("que acesse ao sistema")
    public void acessar_sistema() {
    GerenciadorJava.getDriver().get("https://app.cocobambu.com/delivery");
}

@Quando("Entro no campo perfil")
    public void seleciono_opcao_perfil(){
    po.esperar_elemento_xpath(elementos.btn_perfil);
    po.clicar(elementos.btn_perfil);
}

@Entao("Clico no botao de entrar no perfil")
    public void botao_entrar_perfil() {
    po.esperar_elemento_xpath(elementos.btn_entrar_perfil);
    po.clicar(elementos.btn_entrar_perfil);
    }

@Dado("Digito o E-mail")
    public void digitar_email(){
    po.esperar_elemento_xpath(elementos.input_emailLogin);
//    po.clicar(elementos.input_login);
    po.digitar(elementos.input_emailLogin, "breno.ferreira216@gmail.com");
}

@Quando("Digito a Senha")
    public void digitar_senha(){
    po.esperar_elemento_xpath(elementos.input_senhaLogin);
    po.digitar(elementos.input_senhaLogin, "@Bren01");
}

@Entao("Botao de login")
    public void botao_login(){
    po.esperar_elemento_xpath(elementos.btn_login);
    po.clicar(elementos.btn_login);
}

@Dado("Abrir modal de cadastro")
    public void abrir_modal(){
    po.esperar_elemento_xpath(elementos.btn_cadastro);
    po.clicar(elementos.btn_cadastro);
}

@Quando("prencheer o modal de cadastro")
    public void preencher_modal(){
        po.esperar_elemento_xpath(elementos.input_NomeCompleto);
        po.digitar(elementos.input_NomeCompleto, "Teste Teste");
        po.digitar(elementos.input_emailCadastro, "testeeeeesssss@teste.com");
        po.digitar(elementos.input_senhaCadastro, "@Eobrenin123");
        po.digitar(elementos.input_confirmacaoSenha, "@Eobrenin123");
        po.esperar_elemento_xpath(elementos.combo_select_estado);
        po.clicar(elementos.combo_select_estado);
        po.esperar_elemento_xpath(elementos.combo_select_estado_distritoFederal);
        po.clicar(elementos.combo_select_estado_distritoFederal);
        po.clicar(elementos.checkbox_notificacao);
        po.clicar(elementos.checkbox_termos);
        po.esperar_elemento_xpath(elementos.btn_aceitarTermos);
        po.clicar(elementos.btn_aceitarTermos);
        po.esperar_elemento_xpath(elementos.btn_cadastrar);
        po.clicar(elementos.btn_cadastrar);
    }

@Entao("Validar mensagem de sucesso")
    public void validar_mensagemSucesso(){
        String texto_validacao = po.recuperar_texto_elemento(elementos.texto_sucesso);
        po.validar_mensagem( "O seu CÓDIGO foi enviado para o E-MAIL informado. Caso não encontre na caixa de entrada, verifique na caixa de SPAM e marque em CONFIAR para receber novos e-mails Coco Bambu.", texto_validacao);

}
}
