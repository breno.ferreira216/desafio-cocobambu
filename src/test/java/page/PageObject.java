package page;

import config.GerenciadorJava;
import org.junit.Assert;
import org.junit.jupiter.api.extension.ExecutionCondition;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class PageObject {
    public WebDriverWait wait;
    public WebDriver driver;

    public void clicar(String elemento) {
        try {
            GerenciadorJava.getDriver().findElement(By.id(elemento)).click();
        } catch (org.openqa.selenium.NoSuchElementException e) {
            GerenciadorJava.getDriver().findElement(By.xpath(elemento)).click();
        }
    }

    public void digitar(String elemento, String texto){
        try {
            GerenciadorJava.getDriver().findElement(By.id(elemento)).sendKeys(texto);
        } catch (org.openqa.selenium.NoSuchElementException e) {
            GerenciadorJava.getDriver().findElement(By.xpath(elemento)).sendKeys(texto);
        }
    }

    public void esperar_elemento_xpath(String elemento) {
        wait = new WebDriverWait(GerenciadorJava.getDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elemento)));
    }

    public String esperar_elemento_id(String elemento) {
        wait = new WebDriverWait(GerenciadorJava.getDriver(), 10);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id(elemento)));
        return elemento;
    }

    public String recuperar_texto_elemento(String elemento) {
        wait = new WebDriverWait(GerenciadorJava.getDriver(), 10);
        String texto = wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(elemento))).getText();
        return texto;
    }

    public void validar_mensagem(String texto_esperado, String texto) {
        Assert.assertEquals(texto_esperado, texto);
    }

}
