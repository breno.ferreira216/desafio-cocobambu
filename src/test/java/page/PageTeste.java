package page;

public class PageTeste {

    public String btn_perfil = "//span[contains(text(), 'Perfil')]";
    public String btn_entrar_perfil = "//span[contains(text(), 'Entrar')]";
    public String input_emailLogin = "//input[@placeholder='E-mail']";
    public String input_senhaLogin = "//input[@placeholder='Senha']";
    public String btn_login = "//*[@id=\"content\"]/app-login/div/div/div[2]/button/span";
    public String btn_cadastro = "//span[contains(text(), 'Cadastre-se')]";
    public String input_NomeCompleto = "//input[@placeholder='Nome completo']";
    public String input_emailCadastro = "/html/body/app-root/ion-app/div/div/desktop-modal/div[2]/register-popup/div/form/div[3]/ion-input/input";
    public String input_senhaCadastro = "//*[@id=\"formRegisterUser\"]/div[5]/ion-input/input";
    public String input_confirmacaoSenha = "//input[@placeholder='Confirmar senha']";
    public String combo_select_estado = "//ion-select[@formcontrolname='regionId']";
    public String combo_select_estado_distritoFederal = "/html/body/app-root/ion-app/ion-action-sheet/div[2]/div/div[1]/button[8]";
    public String checkbox_notificacao = "//ion-checkbox[@formcontrolname='customNotification']";
    public String checkbox_termos = "//ion-checkbox[@formcontrolname='termsAndConditions']";
    public String btn_aceitarTermos = "//button[@type='undefined']";
    public String btn_cadastrar = "//span[contains(text(), 'CADASTRAR')]";
    public String texto_sucesso = "//span[contains(text(), ' O seu CÓDIGO foi enviado para o E-MAIL informado. Caso não encontre na caixa de entrada, verifique na caixa de SPAM e marque em CONFIAR para receber novos e-mails Coco Bambu. ')]";
}
