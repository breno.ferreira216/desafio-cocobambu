# language: pt

Funcionalidade: Realizar Cadastro

  Cenario: Abrir a pagina de perfil
    Dado que acesse ao sistema
    Quando Entro no campo perfil
    Entao Clico no botao de entrar no perfil

  Cenario: Realizar um cadastro
    Dado Abrir modal de cadastro
    Quando prencheer o modal de cadastro
    Entao Validar mensagem de sucesso
